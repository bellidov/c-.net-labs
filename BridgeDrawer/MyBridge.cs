using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MatrixGenerator
{
    class MyBridge
    {
    }

    interface IShape
    {
        void Draw(IDrawer d);
        bool ItsMyPoint(int x, int y);
    }

    class Rect : IShape
    {
        int x1, y1, w, h;

        public Rect(int x1, int y1, int w, int h)
        {
            this.x1 = x1;
            this.y1 = y1;
            this.w = w;
            this.h = h;
        }
        public void Draw(IDrawer d)
        {
            d.line(x1, y1, x1 + w, y1);
            d.line(x1, y1 + h, x1 + w, y1 + h);
            d.line(x1, y1, x1, y1 + h);
            d.line(x1 + w, y1, x1 + w, y1 + h);
        }

        public bool ItsMyPoint(int x, int y)
        {
            return (x - this.x1 >= 0 && x - this.x1 <= this.w) && (y - this.y1 >= 0 && y - this.y1 <= h);
        }
    }

    interface IDrawer
    {
        void line(int x1, int y1, int x2, int y2);
    }

    class Drawer : IDrawer
    {
        private Graphics g;
        private Pen p;
        public Drawer(Graphics g, Pen p)
        {
            this.g = g;
            this.p = p;
        }

        virtual public void line(int x1, int y1, int x2, int y2)
        {
            g.DrawLine(p, x1, y1, x2, y2);
        }
        
    }

    class DrawerMini : Drawer
    {
        private IDrawer d;

        public DrawerMini(Graphics g, IDrawer d)
            : base(g, new Pen(Color.Red))
        {
            this.d = d;
        }

        public override void line(int x1, int y1, int x2, int y2)
        {
            this.d.line(100 + x1, 100 + y1, 100 + x2, 100 + y2);
            base.line(x1, y1, x2, y2);
        }
    }
}
