using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MatrixGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            gnm = Graphics.FromHwnd(NMPanel.Handle);
            gsm = Graphics.FromHwnd(SMPanel.Handle);
            p = new Pen(Color.Red);
        }

        Pen p;
        Graphics gnm, gsm;
        IDrawer d;
        List<IShape> shapes = new List<IShape>();

        Graphics gr(Panel p)
        {
            return Graphics.FromHwnd(p.Handle);
        }


        IDrawer normDrawer(Graphics g)
        {
          //  if (d == null)
                d = new Drawer(g, p);

            return d;
        }

        void draw(Graphics g)
        {
            foreach (var s in shapes)
            {
                s.Draw(normDrawer(g));
            }
        }

        private void NMPanel_Paint(object sender, PaintEventArgs e)
        {
        }

        private void NMPanel_MouseClick(object sender, MouseEventArgs e)
        {
   //         draw();
        }

        private void GenButtom_Click(object sender, EventArgs e)
        {
            shapes.Add(new Rect(10, 10, 300, 300));
            draw(gnm);
            draw(gsm);
        }


        private void scheme1_CheckedChanged(object sender, EventArgs e)
        {
            shapes.Add(new Rect(10, 10, 365, 350));
            draw(gnm);
            draw(gsm);
        }

        private void scheme2_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void scheme3_CheckedChanged(object sender, EventArgs e)
        {

        }

        private void scheme4_CheckedChanged(object sender, EventArgs e)
        {
            shapes.Add(new Rect(10, 10, 365, 350));
            draw(gnm);
            draw(gsm);

        }

        private void NMPanel_MouseMove(object sender, MouseEventArgs e)
        {
            SolidBrush solid = new SolidBrush(Color.Red);
            foreach (var s in shapes)
            {
                if (!s.ItsMyPoint(e.X, e.Y))
                {
                    solid.Color = Color.White;
                }
                gnm.FillRectangle(solid, new Rectangle(51, 51, 199, 199));
            }
        }


    }
}
