using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MatrixGenerator
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            
        }

        Pen p;
        Graphics g, gnm, gsm;
        IDrawer d;
        List<IShape> shapes = new List<IShape>();

        bool shadow;
        void myDrawer()
        {
            if (!shadow)
                d = new Drawer(g, p);
            else
            {
                d = new Drawer(g, p);
                d = new ShadowDrw(g, d, p);
            }
        }

        void settings()
        { 
            gnm = Graphics.FromHwnd(NMPanel.Handle);
            gsm = Graphics.FromHwnd(SMPanel.Handle);
            if (scheme1.Checked) { 
                p = new Pen(Color.Blue);
                shadow = false;
            }
            else if (scheme2.Checked)
            {
                p = new Pen(Color.Green);
                shadow = true;
            }
            else if (scheme3.Checked)
            {
                p = new Pen(Color.Brown);
                shadow = false;
            }
            else if (scheme4.Checked) { 
                p = new Pen(Color.Red);
                shadow = true;
            }
        }

        void myBox()
        {
            foreach (var gg in new Graphics[2] { gnm, gsm })
            {
                g = gg;
                g.Clear(Color.White);
                myDrawer(); // sets d with or without shadow
                foreach (var s in shapes)
                {
                    s.Draw(d);
                }
            }
        }

        void drawMatrix()
        {
            if (g != null)
            {
                shapes.Clear();
            }

            //zdes budem ispolzovat poluchennuyu matritsu!
            for (int i = 0; i < 5; i++)
                for (int j = 0; j < 5; j++ )
                //    if (m.readM(i, j) != 0)
                        shapes.Add(new Box(60 + i * 50, 60 + j * 50, 30, 30));



            settings();
            myBox();
        }

        IMatrix m;
        
        private void GenButtom_Click(object sender, EventArgs e)
        {
            // zdes generirovat matritsu!, dalshe sozranit ee
            // 

            m = new NormalMatrix(5, 5);
            InitMatrix.fillMatrix(m, 15, 9);
       //     shapes.Add(new Box(10, 10, 100, 100));
            drawMatrix();
        }


        private void scheme1_CheckedChanged(object sender, EventArgs e)
        {
            drawMatrix();
        }

        private void scheme2_CheckedChanged(object sender, EventArgs e)
        {
            drawMatrix();
        }

        private void scheme3_CheckedChanged(object sender, EventArgs e)
        {
            drawMatrix();
        }

        private void scheme4_CheckedChanged(object sender, EventArgs e)
        {
            drawMatrix();
        }

    }
}
