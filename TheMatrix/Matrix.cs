//listo!

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace mymatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            SparseMatrix m1 = new SparseMatrix(4, 4);
            InitMatrix.fillMatrix(m1, 2, 9);
  //          m1.show();
            MStat st1 = new MStat(m1);
            Console.WriteLine("\nsum of elements = " + st1.sum());
            Console.WriteLine("med of elements = " + st1.med());
            Console.WriteLine("max of elements = " + st1.max());
            Console.WriteLine("not null elements = " + st1.notNulls());

            SparseMatrix m2 = new SparseMatrix(4, 4);
            InitMatrix.fillMatrix(m2, 9, 9);
 //           m2.show();
            MStat st2 = new MStat(m2);
            Console.WriteLine("\nsum of elements = " + st2.sum());
            Console.WriteLine("med of elements = " + st2.med());
            Console.WriteLine("max of elements = " + st2.max());
            Console.WriteLine("not null elements = " + st2.notNulls());
        }
    }

    interface IVector
    {
        int readV(int pos);
        void writeV(int pos, int val);
        int getSize();
    }

    class NormalVector : IVector
    {
        private int size;
        private int[] vector;

        public NormalVector(int _size)
        {
            size = _size;
            vector = new int[_size];
        }

        public int readV(int pos)
        {
            return vector[pos];
        }

        public void writeV(int pos, int val)
        {
            vector[pos] = val;
        }

        public int getSize()
        {
            return size;
        }
    }

    class SparseVector : IVector
    {
        private int size;
        private Dictionary<int, int> map;

        public SparseVector(int _size)
        {
            this.size = _size;
            this.map = new Dictionary<int, int>();
        }

        public int readV(int pos)
        {
            if (map.ContainsKey(pos))
                return map[pos];
            return 0;
        }

        public void writeV(int pos, int val)
        {
            if (map.ContainsKey(pos))
                map[pos] = val;
            else
                map.Add(pos, val);
        }

        public int getSize()
        {
            return this.size;
        }
    }

    interface IMatrix
    {
        int readM(int row, int column);
        void writeM(int row, int column, int value);
        int getSizeRows();
        int getSizeColumns();
    }

    abstract class AMatrix : IMatrix
    {
        private IVector[] rows;

        public AMatrix(int sizeRows, int sizeColumns) 
        {
            this.rows = new IVector[sizeRows];
            for (int i = 0; i < sizeRows; i++ )
                rows[i] = init(sizeColumns);
        }

        protected abstract IVector init(int size);
        
        public int readM(int row, int column)
        {
            return this.rows[row].readV(column);
        }

        public void writeM(int row, int column, int value)
        {
            this.rows[row].writeV(column, value);
        }

        public int getSizeRows()
        {
            return this.rows.Length;
        }

        public int getSizeColumns()
        {
            return this.rows[0].getSize();
        }

        public void show() 
        {
            for (int i = 0; i < this.getSizeRows(); i++)
            {
                for (int j = 0; j < this.getSizeColumns(); j++)
                    Console.Write("{0} ", this.readM(i, j));
                Console.WriteLine();
            }
        }
    }

    class NormalMatrix : AMatrix
    {
        public NormalMatrix(int sizeRows, int sizeColumns)
            : base(sizeRows, sizeColumns)
        {
        }

        protected override IVector init(int size)
        {
            return new NormalVector(size);
        }
    }

    class SparseMatrix : AMatrix
    {
        public SparseMatrix(int sizeRows, int sizeColumns)
            : base(sizeRows, sizeColumns)
        {
        }

        protected override IVector init(int size)
        {
            return new SparseVector(size);
        }
    }

    class InitMatrix
    {
        public static void fillMatrix(IMatrix m, int NotNulls, int MaxValue)
        {
            Random r = new Random();
            int i = 0, p, q;
            while(i < NotNulls){
                p = r.Next(0, m.getSizeRows());
                q = r.Next(0, m.getSizeColumns());

                if (m.readM(p, q) != 0)
                    continue;
                else
                {
                    m.writeM(p, q, r.Next(1, MaxValue));
                    i++;
                }
            }
        }

    }

    class MStat
    {
        private IMatrix m;
        public MStat(IMatrix m)
        {
            this.m = m;
        }

        public int sum()
        {
            int s = 0;
            for (int i = 0; i < m.getSizeRows(); i++)
                for (int j = 0; j < m.getSizeColumns(); j++)
                    s += m.readM(i, j);
             return s;
        }

        public int med()
        {
            return this.sum()/(m.getSizeColumns()*m.getSizeRows());
        }

        public int max()
        {
            int maxx = this.m.readM(0, 0);
            for (int i = 0; i < this.m.getSizeRows(); i++)
                for (int j = 0; j < this.m.getSizeColumns(); j++)
                    if (this.m.readM(i, j) > maxx)
                        maxx = this.m.readM(i, j);
            return maxx;
        }

        public int notNulls()
        {
            int notNull = 0;
            for (int i = 0; i < this.m.getSizeRows(); i++)
                for (int j = 0; j < this.m.getSizeColumns(); j++)
                    if (this.m.readM(i, j) != 0)
                        ++notNull;
            return notNull;
        }
    }

}
