using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Drawing;

namespace MatrixGenerator
{
    class MyBridge
    {
    }

    interface IShape
    {
        void Draw(IDrawer d);
    }

    class Box : IShape
    {
        int x1, y1, w, h;

        public Box(int x1, int y1, int w, int h)
        {
            this.x1 = x1;
            this.y1 = y1;
            this.w = w;
            this.h = h;
        }

        public void Draw(IDrawer d)
        {
            d.line(x1, y1, x1 + w, y1);
            d.line(x1, y1 + h, x1 + w, y1 + h);
            d.line(x1, y1, x1, y1 + h);
            d.line(x1 + w, y1, x1 + w, y1 + h);
        }
    }

    interface IDrawer
    {
        void line(int x1, int y1, int x2, int y2);
    }

    class Drawer : IDrawer
    {
        private Graphics g;
        private Pen p;
        public Drawer(Graphics g, Pen p)
        {
            this.g = g;
            this.p = p;
        }

        virtual public void line(int x1, int y1, int x2, int y2)
        {
            g.DrawLine(p, x1, y1, x2, y2);
        }
        
    }

    class ShadowDrw : Drawer
    {
        private IDrawer d;
    //    private int s;

        public ShadowDrw(Graphics g, IDrawer d, Pen p)
            : base(g, p)
        {
            this.d = d;
        }

        public override void line(int x1, int y1, int x2, int y2)
        {
            this.d.line(5 + x1, 5 + y1, 5 + x2, 5 + y2);
            base.line(x1, y1, x2, y2);
        }
    }
}
