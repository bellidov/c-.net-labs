//This is a not very good solution

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;

namespace mymatrix
{
    class Program
    {
        static void Main(string[] args)
        {
            NormalMatrix m1 = new NormalMatrix(5, 10);
            SparseMatrix m2 = new SparseMatrix(10000, 10000);

            InitMatrix.fillMatrix(m1, 48, 9);
            m1.show();
            
            Console.WriteLine();
            InitMatrix.fillMatrix(m2, 80000000, 9);
         //   m2.show();
        }
    }

    interface IVector
    {
        int readV(int pos);
        void writeV(int pos, int val);
        int getSize();
        IVector init(int col);
    }

    class NormalVector : IVector
    {
        private int size;
        private int[] vector;

        public NormalVector(int _size)
        {
            size = _size;
            vector = new int[_size];
        }

        public int readV(int pos)
        {
            return vector[pos];
        }

        public void writeV(int pos, int val)
        {
            vector[pos] = val;
        }

        public int getSize()
        {
            return size;
        }

        public IVector init(int col)
        {
            return new NormalVector(col);
        }
    }

    class SparseVector : IVector
    {
        private int size;
        private Dictionary<int, int> map;

        public SparseVector(int _size)
        {
            this.size = _size;
            this.map = new Dictionary<int, int>();
        }

        public int readV(int pos)
        {
            if (map.ContainsKey(pos))
                return map[pos];
            return 0;
        }

        public void writeV(int pos, int val)
        {
            if (map.ContainsKey(pos))
                map[pos] = val;
            else
                map.Add(pos, val);
        }

        public int getSize()
        {
            return this.size;
        }

        public IVector init(int col)
        {
            return new SparseVector(col);
        }
    }

    interface IMatrix
    {
        int readM(int row, int column);
        void writeM(int row, int column, int value);
        int getSizeRows();
        int getSizeColumns();
    }

    abstract class AMatrix : IMatrix
    {
        private IVector[] rows;

        public AMatrix(int sizeRows, int sizeColumns, IVector v) 
        {
            this.rows = new IVector[sizeRows];
            for (int i = 0; i < sizeRows; i++ )
                rows[i] = v.init(sizeColumns);
        }

        public int readM(int row, int column)
        {
            return this.rows[row].readV(column);
        }

        public void writeM(int row, int column, int value)
        {
            this.rows[row].writeV(column, value);
        }

        public int getSizeRows()
        {
            return this.rows.Length;
        }

        public int getSizeColumns()
        {
            return this.rows[0].getSize();
        }

        public void show() 
        {
            for (int i = 0; i < this.getSizeRows(); i++)
            {
                for (int j = 0; j < this.getSizeColumns(); j++)
                    Console.Write("{0} ", this.readM(i, j));
                Console.WriteLine();
            }
        }
    }

    class NormalMatrix : AMatrix
    {
        public NormalMatrix(int sizeRows, int sizeColumns)
            : base(sizeRows, sizeColumns, new NormalVector(sizeColumns))
        {
        }
    }

    class SparseMatrix : AMatrix
    {
        public SparseMatrix(int sizeRows, int sizeColumns)
            : base(sizeRows, sizeColumns, new SparseVector(sizeColumns))
        {
        }
    }

    class InitMatrix
    {
        public static void fillMatrix(IMatrix m, int NotNulls, int MaxValue)
        {
            Random r = new Random();
            int i = 0, p, q;
            while(i < NotNulls){
                p = r.Next(0, m.getSizeRows());
                q = r.Next(0, m.getSizeColumns());

                if (m.readM(p, q) != 0)
                    continue;
                else
                {
                    m.writeM(p, q, r.Next(1, MaxValue));
                    i++;
                }
            }
        }

    }

}
